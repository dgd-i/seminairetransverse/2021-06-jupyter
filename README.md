# 20-minute introduction to Notebooks

## Running on binder (no install needed!)

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fdgd-i%2Fseminairetransverse%2F2021-06-jupyter.git/HEAD?urlpath=%2Flab)

## Running the example locally

From a python virtual environment or conda environment with python installed, run

```bash
pip install -r requirements.txt
```

To launch Jupyterlab, run

```bash
jupyter lab
```

## About the example data

Data used in the example are described in the following article:
- Truong, C., Barrois-Müller, R., Moreau, T., Provost, C., Vienne-Jumeau, A., Moreau, A., Vidal, P.-P., Vayatis, N., Buffat, S., Yelnik, A., Ricard, D., & Oudre, L. (2019). A data set for the study of human locomotion with inertial measurements units. Image Processing On Line (IPOL), 9. [[abstract]](https://deepcharles.github.io/publication/ipol-data-2019) [[doi]](https://doi.org/10.5201/ipol.2019.265) [[pdf]](http://deepcharles.github.io/files/ipol-walk-data-2019.pdf) [[online demo]](http://ipolcore.ipol.im/demo/clientApp/demo.html?id=265)

The whole dataset can be downloaded under the terms of CC-BY-NC-SA license, see the [github project page](https://github.com/deepcharles/gait-data) for instructions.